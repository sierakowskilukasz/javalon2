package com.sda.java.algorithms;

public class CountingSort {

    public int[] sort(int[] unsortedArray) {
        int max = findMax(unsortedArray);
        int[] countArray = new int[max]; // countArray - niebieska tablica

        for (int i = 0; i < unsortedArray.length; i++) {
            int index = unsortedArray[i] - 1; // values - czarna tablica
            countArray[index]++;
            // countArray[index] = countArray[index] + 1;
        }
        return prepareResult(countArray, unsortedArray.length);
    }

    private int[] prepareResult(int[] countArray, int length) {
        int[] sortedArray = new int[length]; // tablica zielona
        int position = 0;
        for (int i = 0; i < countArray.length; i++) {
            for (int j = 0; j < countArray[i]; j++) {
                sortedArray[position] = i + 1;
                position++;
            }
        }
        return sortedArray;
    }

    public int findMax(int[] values) {
        int max = Integer.MIN_VALUE;
        for (int value : values) {
            if (value > max) {
                max = value;
            }
        }
        return max;
    }
}
