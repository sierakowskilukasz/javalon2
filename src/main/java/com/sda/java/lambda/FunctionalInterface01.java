package com.sda.java.lambda;

@FunctionalInterface
public interface FunctionalInterface01 {
    boolean isGreater(int x1, int x2);
}
