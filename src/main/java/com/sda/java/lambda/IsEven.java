package com.sda.java.lambda;

@FunctionalInterface
public interface IsEven {
    boolean check(int number);
}
