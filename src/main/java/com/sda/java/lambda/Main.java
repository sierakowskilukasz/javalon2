package com.sda.java.lambda;

public class Main {
    public static void main(String[] args) {
        FunctionalInterface01 fi = (x1, x2) -> x1 > x2;
        System.out.println(fi.isGreater(4, 3));
        System.out.println(fi.isGreater(1, 3));

        IsEven isEven = x -> {
            int remainder = x % 2;
            return remainder == 0;
        };

        System.out.println(isEven.check(15));
        System.out.println(isEven.check(10));
    }
}
