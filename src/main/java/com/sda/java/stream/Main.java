package com.sda.java.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 44, 33, 22, 55, 32);
        List<Integer> finalList = numbers.stream().filter(n -> n > 40).map(n -> n * 2).collect(Collectors.toList());
        finalList.forEach(element -> System.out.println(element));

        // Wyszukiwanie największej wartości w liście za pomocą strumienia
        int max = numbers.stream().mapToInt(n -> n).max().getAsInt();
        // Wyszukiwanie najmniejszej wartości w liście za pomocą strumienia
        int min = numbers.stream().mapToInt(n -> n).min().getAsInt();

        //        Powyższa lambda zapisana w legacy Code
        //        List<Integer> result = new ArrayList<>();
        //        for (int i = 0; i < numbers.size(); i++) {
        //            if (numbers.get(i) > 40) {
        //                result.add(numbers.get(i));
        //            }
        //        }
        //        for (int i = 0; i < result.size(); i++) {
        //            result.set(i, result.get(i) * 2);
        //        }
        //        for (int i = 0; i < result.size(); i++) {
        //            System.out.println(result.get(i));
        //        }
    }
}
