package com.sda.java.threads.interfaceExample;

public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            Thread thread = new Thread(new MyThread());
            thread.setName("MyThread-" + i);
            thread.start();
        }
    }
}
