package com.sda.java.threads.interfaceExample;

import java.time.LocalDateTime;

public class MyThread implements Runnable {
    private static final int SLEEP_TIME = 1000;
    private String name;

    public MyThread() {
    }

    public MyThread(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(name + ": " + LocalDateTime.now());
            System.out.println(name + ": " + i);
            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
