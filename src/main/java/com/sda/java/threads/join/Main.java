package com.sda.java.threads.join;

import com.sda.java.threads.interfaceExample.MyThread;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Thread myThread1 = new Thread(new MyThread("Wątek 1"));
        Thread myThread2 = new Thread(new MyThread("Wątek 2"));
        myThread1.start();
        myThread1.join(2000);
        myThread2.start();
    }
}
