package com.sda.java.threads.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sda.java.threads.interfaceExample.MyThread;

public class Main {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        executorService.execute(new MyThread());
        executorService.execute(new MyThread());
        executorService.shutdown();
    }
}
