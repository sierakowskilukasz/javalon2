package com.sda.java.algorithms;

import org.junit.Assert;
import org.junit.Test;

public class BubbleSortTest {

    @Test
    public void sort() {
        int[] unsortedArray = { 1, 9, 3, 7, 10, 3 };
        int[] expectedArray = { 1, 3, 3, 7, 9, 10 };

        BubbleSort bubbleSort = new BubbleSort();
        int[] sa = bubbleSort.sort(unsortedArray);

        Assert.assertArrayEquals(expectedArray, sa);
    }
}