package com.sda.java.algorithms;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CountingSortTest {

    @Test
    public void sort() {
        int[] unsortedArray = { 1, 9, 3, 7, 10, 3 };
        int[] expectedArray = { 1, 3, 3, 7, 9, 10 };

        CountingSort countingSort = new CountingSort();
        int[] sa = countingSort.sort(unsortedArray);

        Assert.assertArrayEquals(expectedArray, sa);
    }
}